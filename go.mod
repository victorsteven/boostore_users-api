module bookstore_users-api

go 1.13

require (
	bookstore_oauth-go v0.0.0
	bookstore_utils-go v0.0.0
	github.com/gin-gonic/gin v1.4.0
	github.com/go-sql-driver/mysql v1.4.1
	github.com/joho/godotenv v1.3.0
	go.uber.org/zap v1.13.0
)

replace bookstore_oauth-go v0.0.0 => ../bookstore_oauth-go

replace bookstore_utils-go v0.0.0 => ../bookstore_utils-go
