package mysql_utils

import (
	"bookstore_utils-go/rest_errors"
	"errors"
	"github.com/go-sql-driver/mysql"
	"strings"
)

const (
	indexUniqueEmail = "email_UNIQUE"
	ErrorNoRows = "no rows in result set"
)

func ParseError(err error) rest_errors.RestErr {
	//convert the error to a mysql error
	sqlErr, ok := err.(*mysql.MySQLError)
	if !ok {
		if strings.Contains(err.Error(), ErrorNoRows) {
			return rest_errors.NewNotFoundError("no record matching given id")
		}
		return rest_errors.NewInternalServerError("error parsing database response", err)
	}
	switch sqlErr.Number {
	case 1062:
		return rest_errors.NewBadRequestError("email already taken")
	}
	 return rest_errors.NewInternalServerError("error when processing request", errors.New("database error"))
}