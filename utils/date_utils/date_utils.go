package date_utils

import "time"

const (
	apiDateLayout = "2006-01-02T15:04:05Z" //Z - standard time zone
	apiDBLayout = "2006-01-02 15:04:05" // in datetime format
)

func GetNow() time.Time {
	return 	time.Now().UTC()
}
func GetNowString() string {
	return GetNow().Format(apiDateLayout)
}
func GetNowDBFormat() string {
	return GetNow().Format(apiDBLayout)
}

