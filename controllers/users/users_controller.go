package users

import (
	"bookstore_oauth-go/oauth"
	"bookstore_users-api/domain/users"
	"bookstore_users-api/services"
	"bookstore_utils-go/rest_errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func getUserId(userIdParam string) (int64, rest_errors.RestErr) {
	userId, userErr := strconv.ParseInt(userIdParam, 10, 64)
	if userErr != nil {
		return 0, rest_errors.NewBadRequestError("user id should be a number")
	}
	return userId, nil
}

func Get(c *gin.Context){
	if err := oauth.AuthenticateRequest(c.Request); err != nil {
		c.JSON(err.Status(), err)
		return
	}
	//if callerId := oauth.GetCallerId(c.Request); callerId == 0 {
	//	err := errors.RestErr{
	//		Message: "resource not available",
	//		Status:  http.StatusUnauthorized,
	//	}
	//	c.JSON(err.Status, err)
	//	return
	//}
	userId, err := getUserId(c.Param("user_id"))
	if err != nil {
		c.JSON(err.Status(), err)
		return
	}
	user, getErr := services.UsersService.GetUser(userId)
	if getErr != nil {
		c.JSON(getErr.Status(), getErr)
		return
	}
	//if the owner of the access token sent is the same as the user query it,
	if oauth.GetCallerId(c.Request) == user.Id {
		c.JSON(http.StatusOK, user.Marshal(false))
		return
	}
	//c.JSON(http.StatusOK, user)
	c.JSON(http.StatusOK, user.Marshal(oauth.IsPublic(c.Request)))

}

func Create(c *gin.Context){
	var user users.User
	//bytes, err := ioutil.ReadAll(c.Request.Body)
	//if err != nil {
	//	return
	//}
	//if err := json.Unmarshal(bytes, &user); err != nil {
	//	fmt.Println(err.Error())
	//	return
	//}
	if err := c.ShouldBindJSON(&user); err != nil {
		restErr := rest_errors.NewBadRequestError("invalid json body")
		c.JSON(restErr.Status(), restErr)
		return
	}

	result, restErr := services.UsersService.CreateUser(user)
	if restErr != nil {
		c.JSON(restErr.Status(), restErr)

		return
	}
	c.JSON(http.StatusCreated, result.Marshal(c.GetHeader("X-Public") == "true"))
}



func Update(c *gin.Context) {
	userId, err := getUserId(c.Param("user_id"))
	if err != nil {
		c.JSON(err.Status(), err)
		return
	}
	var user users.User
	if err := c.ShouldBindJSON(&user); err != nil {
		restErr := rest_errors.NewBadRequestError("invalid json body")
		c.JSON(restErr.Status(), restErr)
		return
	}
	user.Id = userId

	isPartial := c.Request.Method == http.MethodPatch
	result, err := services.UsersService.UpdateUser(isPartial, user)
	if err != nil {
		c.JSON(err.Status(), err)
		return
	}
	c.JSON(http.StatusOK, result.Marshal(c.GetHeader("X-Public") == "true"))
}

func Delete(c *gin.Context) {
	userId, err := getUserId(c.Param("user_id"))
	if err != nil {
		c.JSON(err.Status(), err)
		return
	}
	if err := services.UsersService.DeleteUser(userId); err != nil {
		c.JSON(err.Status(), err)
		return
	}
	c.JSON(http.StatusOK, map[string]string{"status": "deleted"})
}

func Search(c *gin.Context) {
	status := c.Query("status")
	users, err := services.UsersService.SearchUser(status)
	if err != nil {
		c.JSON(err.Status(), err)
		return
	}
	result := make([]interface{}, len(users))
	for index, user := range users {
		result[index] = user.Marshal(c.GetHeader("X-Public") == "true")
	}
	//users.Marshal()
	c.JSON(http.StatusOK, users.Marshal(c.GetHeader("X-Public") == "true"))
}

func Login(c *gin.Context) {
	var request users.LoginRequest
	if err := c.ShouldBindJSON(&request); err != nil {
		restErr := rest_errors.NewBadRequestError("invalid json body")
		c.JSON(restErr.Status(), restErr)
		return
	}
	user, err := services.UsersService.LoginUser(request)
	if err != nil {
		fmt.Println("this is a login err: ", err)
		//errResponse
		loginErr := rest_errors.NewRestError("invalid json body", http.StatusUnauthorized, "the error", nil)
		fmt.Println("this is the logger man: ", loginErr)
		c.JSON(err.Status(), loginErr)
		return
	}
	//c.JSON(http.StatusOK, user)
	c.JSON(http.StatusOK, user.Marshal(c.GetHeader("X-Public") == "true"))

}