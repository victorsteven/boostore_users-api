package app

import (
	"bookstore_utils-go/logger"
	"github.com/gin-gonic/gin"
)

var (
	router = gin.Default()
)

func StartApp(){
	mapUrls()
	logger.Info("about to start the app")
	router.Run(":8888")
}
