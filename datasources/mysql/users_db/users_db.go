package users_db

import (
	"bookstore_utils-go/logger"
	"database/sql"
	"fmt"
	"github.com/go-sql-driver/mysql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
	"log"
	"os"
)

var (
	Client *sql.DB //let assign the users db once here
)

func init(){

	if err := godotenv.Load(); err != nil {
		log.Print("sad .env file found")
	}
	username := os.Getenv("mysql_username")
	password := os.Getenv("mysql_password")
	host := os.Getenv("mysql_host")
	schema := os.Getenv("mysql_schema")

	dataSourceName := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8", username, password, host, schema)
	var err error
	Client, err = sql.Open("mysql", dataSourceName)
	if err != nil {
		panic(err)
	}
	if err = Client.Ping(); err != nil {
		panic(err)
	}
	mysql.SetLogger(logger.GetLogger())
	log.Println("database successfully configured")

}